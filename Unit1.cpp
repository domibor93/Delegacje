//---------------------------------------------------------------------------

#include <vcl.h>              //domyslna borlandowska biblioteka (na niej praktycznie borland chodzi)
#pragma hdrstop
#include "Unit1.h"
#include "Nocleg.h"
#include "Osoba.h"
#include "Transport.h"
#include "Delegacja.h"
#include <vector.h>
#include <fstream>      //biblioteka do obs�ugi plik�w
#include <string.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "CGRID"
#pragma resource "*.dfm"


using namespace std;

TForm1 *Form1;

Osoba o(1, "Bezimienny", "Annonim");
Delegacja d;
vector < Delegacja > vecDelegacje;          // wektory Delegacji i pracownikow
vector < Osoba >     vecOsoby;              // Delegacja jest przyjacielem Osoby
                                            // na razie sa spi�te na sztywno- wyciagane sa dane wg jednego indeksu
                                            // ale mozna przebudowac program zeby osoby mialy osobna baze, i dodatkowe informacje
int index=0;                              //index wskazujacy w ktorym miejscu vectora sie znajdujemy
double ustawowaDieta=30;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
         Form1->Init();
}
//---------------------------------------------------------------------------




void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{
         sgTransport->Cells[sgTransport->Col][sgTransport->Row] = ComboBox1->Items->Strings[ComboBox1->ItemIndex];
}
//---------------------------------------------------------------------------

void __fastcall TForm1::sgTransportClick(TObject *Sender)
{
        if( sgTransport->Col == 1 )                             //ustawianie comboboxa do kolumny Srodek transportu
         {
             TRect Recto     = sgTransport->CellRect(sgTransport->Col, sgTransport->Row);
                ComboBox1->Top  = sgTransport->Top;
                ComboBox1->Left = sgTransport->Left;
                ComboBox1->Top  = ComboBox1->Top + Recto.Top + sgTransport->GridLineWidth;
                ComboBox1->Left = ComboBox1->Left + Recto.Left + sgTransport->GridLineWidth + 1;
                ComboBox1->Height = (Recto.Bottom - Recto.Top) + 1;
                ComboBox1->Width  = Recto.Right - Recto.Left;
                ComboBox1->Visible = True;
         }
         else
                  ComboBox1->Visible = False;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)  //Dodawanie Nowego Wiersza do string grida
{
        sgTransport->RowCount++;
              for(int i = sgTransport->RowCount - 1; i >= sgTransport->Row + 1; i--)
              {
                 for(int j = 1; j < sgTransport->ColCount; j++)
                 {
                   if(i == sgTransport->Row + 1)
                         sgTransport->Cells[j][sgTransport->Row + 1] = "";
                     else
                         sgTransport->Cells[j][i] = sgTransport->Cells[j][i - 1];
                  }

              }
              sgTransport->Cells[0][sgTransport->RowCount-1] = sgTransport->RowCount-1;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditDietaExit(TObject *Sender)
{
    double value;
    if( !TryStrToFloat(EditDieta->Text, value) )
    {                                                             //nale�y wpisac z przecinkiem, NIE kropk�
        Application->MessageBox("Wprowadz liczb� typu double.\nU�yj przecinka jako separatora.","Nieprawid�owa warto��", MB_OK | MB_ICONASTERISK);
        EditDieta->SetFocus();
        return;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit5Exit(TObject *Sender)
{
    double value;                                                   //value- tymczasowa zmienna uzywana tylko do sprawdzenia
    if( !TryStrToInt(Edit5->Text, value) )                          //czy wprowadzono poprawny format liczby
    {                                                             //nale�y wpisac z przecinkiem, NIE kropk�
        Application->MessageBox("Wprowadz liczb� typu ca�kowitego.","Nieprawid�owa warto��", MB_OK | MB_ICONASTERISK);
        Edit5->SetFocus();
        return;
    }        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::EdNocFakturaExit(TObject *Sender)
{
    double value;
    if( !TryStrToFloat(EdNocFaktura->Text, value) )
    {                                                             //nale�y wpisac z przecinkiem, NIE kropk�
        Application->MessageBox("Wprowadz liczb� typu double.\nU�yj przecinka jako separatora.","Nieprawid�owa warto��", MB_OK | MB_ICONASTERISK);
        EdNocFaktura->SetFocus();
        return;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::sgTransportExit(TObject *Sender)
{
    double sumaTransport=0;    //sumowanie kosztow transportu
                for(int k=1; k < sgTransport->RowCount; k++)
                   {
                           sumaTransport+=(sgTransport->Cells[2][k]=="")? 0:StrToFloat(sgTransport->Cells[2][k]); // sprawdzanie czy pusta
                           Edit8->Text=sumaTransport;                                                            //komorka, jezeli tak to =0
                  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
 if(!vecDelegacje.empty())
      {
        index=Edit1->Text.ToInt()-1;

        if(index>=vecDelegacje.size() || index<0)
           index=vecDelegacje.size()-1;

        d=vecDelegacje[index];
        o=vecOsoby[index];
        Wczytaj();
        }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button3Click(TObject *Sender)
{
        index=Edit1->Text.ToInt()-1;
        Edit1->ReadOnly=false;
        Zapisz();
        WylEdycje();
        if(index<vecDelegacje.size())
           {
                vecDelegacje[index]=d;
                vecOsoby[index]=o;
           }
        else
           {
                vecDelegacje.push_back(d);
                 vecOsoby.push_back(o);
           }
           ZapiszDoPliku();

}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button9Click(TObject *Sender)
{
     if(sgTransport->RowCount>2)
     {
        for(int i = sgTransport->Row; i <= sgTransport->RowCount - 1; i++)
         {
          for(int y = 0; y < sgTransport->ColCount; y++)
          {
           sgTransport->Cells[y][i] = sgTransport->Cells[y][i + 1];
           sgTransport->Cells[y][i + 1] = "";
          }
         }
         sgTransport->RowCount--;
     }
}
//---------------------------------------------------------------------------



void __fastcall TForm1::Edit1Exit(TObject *Sender)
{
    double value;
    if( !TryStrToInt(Edit1->Text, value) )
    {                                                             //nale�y wpisac z przecinkiem, NIE kropk�
        Application->MessageBox("Wprowadz liczb� typu ca�kowitego.\n","Nieprawid�owa warto��", MB_OK | MB_ICONASTERISK);
        Edit1->SetFocus();
        return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Init()
{
        sgTransport->Cells[0][1]=1;
        DateTime_SetFormat( dtpGodzinaOd->Handle, "HH:mm" ); // ustawianie formatu godziny na hh:mm, bo domyslnie sa jeszcze sekundy
        DateTime_SetFormat( dtpGodzinaDo->Handle, "HH:mm" );
        sgTransport->Cells[0][0] = "Nr";
        sgTransport->Cells[1][0] = "�rodek Transporu";
        sgTransport->Cells[2][0] = "Cena";
        Edit6->ReadOnly=true; //te edity sluza tylko do wyswietlania czasu trwania delegacji
        Edit9->ReadOnly=true;
        Edit10->ReadOnly=true;
        Edit11->ReadOnly=true; //ten edit wysietla tylko sume zwrotu wiec nie mozna edytowac
        EditDieta->Text=FloatToStr(ustawowaDieta);
        rbWyzPosilki->Checked=true;
        rbRyczalt->Checked=true; 
        
        //wylaczenie mozliwosci edycji do czasu kliniecia opcji nowy lub edytuj:
        WylEdycje();
        WczytajZpliku();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Zapisz()                                //zeby za kazdym razem wyciagajac z obiektu nie trzeba bylo pisac tego wywolania
{
        o.SetOsoba(Edit5, Edit3, Edit4);
     d.SetDel(o,
            Edit1, DateTimePicker1, DateTimePicker2, dtpGodzinaOd, dtpGodzinaDo, Edit2, Uwagi,
            EditDieta ,sgTransport, rbFaktura,
            EdNocFaktura, rbWyzPosilki, rbWyzWlasne, Edit7,
            Edit5, Edit3 , Edit4
            );
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Wczytaj()
{
             d.PokazDel(o,
                   Edit1, DateTimePicker1, DateTimePicker2, dtpGodzinaOd, dtpGodzinaDo,
                   Edit6, Edit9, Edit10,
                   Edit2, Uwagi,
                   EditDieta , sgTransport, rbFaktura,
                   rbRyczalt, EdNocFaktura,  edNoclegRes,
                   rbWyzPosilki, rbWyzWlasne, Edit7,
                   Edit5, Edit3 , Edit4, Edit11
                   );
}
//---------------------------------------------------------------------------

void __fastcall TForm1::WylEdycje()         //
{
        gbDelegowany->Enabled=false;
        gbNocleg->Enabled=false;
        GroupBox1->Enabled=false;
        gbTransport->Enabled=false;
        DateTimePicker1->Enabled=false;
        DateTimePicker2->Enabled=false;
        dtpGodzinaOd->Enabled=false;
        dtpGodzinaDo->Enabled=false;
        Edit2->Enabled=false;       //cel
        Button5->Enabled=false;     //przycisk licz
        EditDieta->Enabled=false;
        Uwagi->Enabled=false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WlEdycje()         //
{
        gbDelegowany->Enabled=true;
        gbNocleg->Enabled=true;
        GroupBox1->Enabled=true;
        gbTransport->Enabled=true;
        DateTimePicker1->Enabled=true;
        DateTimePicker2->Enabled=true;
        dtpGodzinaOd->Enabled=true;
        dtpGodzinaDo->Enabled=true;
        Edit2->Enabled=true;
        Button5->Enabled=true;
        EditDieta->Enabled=true;
        Uwagi->Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
     if(!vecDelegacje.empty())
      {
        WlEdycje();
        Edit1->ReadOnly=true;

       }
       else
       {
        Application->MessageBox("Nie ma zadnej delegacji w bazie", "Pusta baza", MB_OK | MB_ICONASTERISK);
       }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        for(int i=sgTransport->RowCount-1; i>0; i--)   //czyszczenie string grida
		{
			for (int j=0; j<sgTransport->ColCount; j++)
			{
				sgTransport->Cells[j][i]="";
				
			}		
			if(i>1)
				sgTransport->RowCount--;
			
		}
                sgTransport->Cells[0][1]=1;
                
    WlEdycje();
    DateTimePicker1->SetFocus();
    Edit1->Text=vecDelegacje.size()+1;
    Edit1->ReadOnly=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::gbDelegacjaExit(TObject *Sender)
{
     Zapisz();
     Wczytaj(); // wczytuje od nowa bo wewnatrz zapisu liczy niektore parametry
}
//---------------------------------------------------------------------------

void __fastcall TForm1::gbDelegowanyExit(TObject *Sender)
{
     Zapisz();
     Wczytaj(); // wczytuje od nowa bo wewnatrz zapisu liczy niektore parametry
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GroupBox1Exit(TObject *Sender)
{
     Zapisz();
     Wczytaj(); // wczytuje od nowa bo wewnatrz zapisu liczy niektore parametry        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::gbTransportExit(TObject *Sender)
{
     Zapisz();
     Wczytaj(); // wczytuje od nowa bo wewnatrz zapisu liczy niektore parametry
}
//---------------------------------------------------------------------------


void __fastcall TForm1::gbNoclegExit(TObject *Sender)
{
     Zapisz();
     Wczytaj(); // wczytuje od nowa bo wewnatrz zapisu liczy niektore parametry
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
     Zapisz();
     Wczytaj();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ZapiszDoPliku ()          //
{

  int rowtmp=0;         //zmienna pomocnicza do zapisania grida, przechowuje liczbe wierszy
  // FILE *plik; //tworzenie obiektu pliku
  fstream plik;
 // plik = fopen(Edit1->Text.c_str(), "r+"); //otwieranie pliku w trybie do zapisu
                                         //cls bo sie ladnie w excelu wyswietla

   plik.open("delegacja.cls", ios::out);
   if(!(plik==NULL)) //jezeli plik istnieje
   {
     for (int i=0; i<vecDelegacje.size(); ++i) //petla dla wszystkich delegacji w wektorze
     {
          d=vecDelegacje[i];
          rowtmp=sgTransport->RowCount;
          Wczytaj();
          plik << Edit1->Text.c_str();      //.c_str() - konwertuje na string bo borland uzywa AnsiStringa
          plik << "\t";   // zapisywanie tabulatora do pliku - do rozdzielenia komorek w pliku cls
          plik << DateTimePicker1->Date.DateString().c_str();
          plik << "\t";
          plik << DateTimePicker2->Date.DateString().c_str();
          plik << "\t";
          plik << dtpGodzinaOd->Date.DateString().c_str();
          plik << "\t";
          plik << dtpGodzinaDo->Date.DateString().c_str();
          plik << "\t";
          plik << EditDieta->Text.c_str();
          plik << "\t";
          plik << UsunSpacje(Edit2->Text.c_str());
          plik << "\t";
          plik << UsunSpacje(Uwagi->Text.c_str());     //zamiana spacji na _
          plik << "\t";
          plik << Edit5->Text.c_str();
          plik << "\t";
          plik << Edit3->Text.c_str();
          plik << "\t";
          plik << Edit4->Text.c_str();
          plik << "\t";
          plik << rbWyzPosilki->Checked;
          plik << "\t";
          plik << rbWyzWlasne->Checked;
          plik << "\t";
          plik << Edit7->Text.c_str();
          plik << "\t";
          plik << rbFaktura->Checked;
          plik << "\t";
          plik << rbRyczalt->Checked;
          plik << "\t";
          plik << EdNocFaktura->Text.c_str();
          plik << "\t";
          plik << edNoclegRes->Text.c_str();
          plik << "\t";
          //sgTransport do pliku
          plik << rowtmp;
          plik << "\t";
          for (int j=1; j<rowtmp; j++)
          {
             for (int k =1; k<3; k++)
             {
               plik << UsunSpacje(sgTransport->Cells[k][j].c_str());
               plik << "\t";
             }
          }
          //
          plik << Edit8->Text.c_str();
          plik << "\t";
          plik << Edit11->Text.c_str();
          if(i != vecDelegacje.size()-1)
            plik << "\n";  //dla ostatniego elementu nie zapisuj znaku nowej lini
     }
     plik.close(); // zamykanie pliku
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WczytajZpliku ()
{

  
  int rowtmp=0;
  fstream plik;
																	 //cls bo sie ladnie w excelu wyswietla
   plik.open("delegacja.cls", ios::in); // otwarcie w trybie do odczytu
   if(!(plik==NULL)) //jezeli plik istnieje
   {
     while(!(plik.eof()))
     {
          string a;
          plik >> a;
          Edit1->Text=a.c_str();      //.c_str() - konwertuje na string bo borland uzywa AnsiStringa
          plik >> a;
          DateTimePicker1->Date.DateString()=a.c_str();;
          plik >> a;
          DateTimePicker2->Date.DateString()=a.c_str();;
          plik >> a;
          dtpGodzinaOd->Date.DateString()=a.c_str();;
          plik >> a;
          dtpGodzinaDo->Date.DateString()=a.c_str();;
          plik >> a;
          EditDieta->Text=a.c_str();;
          plik >> a;
          a=PrzywrocSpacje(a);
          Edit2->Text=a.c_str();
          plik >> a;
          a=PrzywrocSpacje(a);
          Uwagi->Text=a.c_str();
          plik >> a;
          Edit5->Text=a.c_str();;
          plik >> a;
          Edit3->Text=a.c_str();;
          plik >> a;
          Edit4->Text=a.c_str();
          plik >> rbWyzPosilki->Checked;
          plik >> rbWyzWlasne->Checked;
          plik >> a;
          Edit7->Text=a.c_str();;
          plik >> rbFaktura->Checked;
          plik >> rbRyczalt->Checked;
          plik >> a;
          EdNocFaktura->Text=a.c_str();;
          plik >> a;
          edNoclegRes->Text=a.c_str();;
          //sgTransport do pliku
          plik >> rowtmp;
          
          for (int j=1; j<rowtmp; j++)
          {
             for (int k =1; k<3; k++)
             {
               plik >> a;
               a=PrzywrocSpacje(a);
               sgTransport->Cells[k][j]=a.c_str();
             }
             sgTransport->Cells[0][j]=j;
             if(j!=rowtmp-1)
             sgTransport->RowCount++;
          }
          //
          plik >> a;
          Edit8->Text=a.c_str();;
          plik >> a;
          Edit11->Text=a.c_str();;

          Zapisz();  //zapisuje z formatki do obiektow
          vecDelegacje.push_back(d);
          vecOsoby.push_back(o);
     }
     plik.close(); // zamykanie pliku
   }
}

void __fastcall TForm1::Button10Click(TObject *Sender)
{
        Edit1->ReadOnly=false;
        WylEdycje();
        if(!vecDelegacje.empty())    //jezeli vektor nie jest pusty
        {
                index=vecDelegacje.size()-1;  // jezeli odrzucimy zmiany wyswietla ostatnia delegacje z bazy
                d=vecDelegacje[index];
                o=vecOsoby[index];
                Wczytaj();
        }
}
//---------------------------------------------------------------------------

string __fastcall TForm1::PrzywrocSpacje(string str)
{
     vector <int> index;        //tymczasowy wektor indeksow
     char to_delete='_';
     int pos= -1; //zmenna tymczasowa do indeksowania
     pos = str.find(to_delete);
      if( pos == (-1) ) //jezeli nie ma tego znaku w stringu
    {
        return str; //zwroc ten string
    }
    else
	{
		do // start loop
		{
			index.push_back(pos); // dodaj index to wektora
			pos = str.find( to_delete, pos + 1 ); // znajdz nastepny indeks
		} while( pos != -1 ); //petla dziala do czasu jak pos != -1

		for(int i = 0 ; i < index.size() ; ++i) //petla dla wsyzstkich indeksow
		{
			str[index[i]] = ' '; //zmie� snak
		}
		index.clear(); // clear temporary vector
		return str; // return proper string
	}

}

string __fastcall TForm1::UsunSpacje(string str)
{

        vector <int> index;
	char to_delete = ' '; //znak od usuniecia
	int pos = -1; // temporary variable for index
	pos = str.find(to_delete);
    if( pos == (-1) ) // jezeli znaku nie ma w stringu to go nie zmieniaj
    {
        return str;
    }
	else
	{
		do // start loop
		{
			index.push_back(pos);
			pos = str.find( to_delete, pos + 1 );
		} while( pos != -1 );

		for(int i = 0 ; i < index.size() ; ++i)
		{
			str[index[i]] = '_'; //zamien stak
		}
		index.clear(); // clear wektor
		return str; //
	}
}

object Form1: TForm1
  Left = 133
  Top = 191
  Width = 1029
  Height = 728
  AutoSize = True
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Delegacja'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbDelegacja: TGroupBox
    Left = 0
    Top = 0
    Width = 1013
    Height = 281
    Align = alTop
    Caption = 'Delegacja'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnExit = gbDelegacjaExit
    object Label1: TLabel
      Left = 8
      Top = 30
      Width = 75
      Height = 16
      Caption = 'Nr Delegacji'
    end
    object Label2: TLabel
      Left = 32
      Top = 72
      Width = 18
      Height = 16
      Caption = 'Od'
    end
    object Label3: TLabel
      Left = 32
      Top = 104
      Width = 18
      Height = 16
      Caption = 'Do'
    end
    object Label4: TLabel
      Left = 160
      Top = 72
      Width = 32
      Height = 16
      Caption = 'Godz'
    end
    object Label5: TLabel
      Left = 160
      Top = 104
      Width = 32
      Height = 16
      Caption = 'Godz'
    end
    object Label7: TLabel
      Left = 32
      Top = 160
      Width = 20
      Height = 16
      Caption = 'Cel'
    end
    object Label8: TLabel
      Left = 24
      Top = 216
      Width = 38
      Height = 16
      Caption = 'Uwagi'
    end
    object Label12: TLabel
      Left = 440
      Top = 152
      Width = 32
      Height = 16
      Caption = 'Dieta'
    end
    object Label13: TLabel
      Left = 576
      Top = 48
      Width = 134
      Height = 16
      Caption = 'Czas trwania delegacji'
    end
    object Label6: TLabel
      Left = 760
      Top = 49
      Width = 18
      Height = 16
      Caption = 'dni'
    end
    object Label14: TLabel
      Left = 936
      Top = 48
      Width = 21
      Height = 16
      Caption = 'min'
    end
    object Label15: TLabel
      Left = 840
      Top = 48
      Width = 40
      Height = 16
      Caption = 'godzin'
    end
    object Label16: TLabel
      Left = 624
      Top = 240
      Width = 152
      Height = 24
      Caption = 'Razem do zap'#322'aty'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DateTimePicker1: TDateTimePicker
      Left = 64
      Top = 64
      Width = 89
      Height = 24
      CalAlignment = dtaLeft
      Date = 42371
      Time = 42371
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 0
    end
    object DateTimePicker2: TDateTimePicker
      Left = 64
      Top = 104
      Width = 89
      Height = 24
      CalAlignment = dtaLeft
      Date = 42371
      Time = 42371
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 1
    end
    object dtpGodzinaOd: TDateTimePicker
      Left = 200
      Top = 64
      Width = 81
      Height = 24
      CalAlignment = dtaLeft
      Date = 0.12131547449826
      Time = 0.12131547449826
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkTime
      ParseInput = False
      TabOrder = 2
    end
    object dtpGodzinaDo: TDateTimePicker
      Left = 200
      Top = 96
      Width = 81
      Height = 24
      CalAlignment = dtaLeft
      Date = 0.12131547449826
      Time = 0.12131547449826
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkTime
      ParseInput = False
      TabOrder = 3
    end
    object Edit2: TEdit
      Left = 77
      Top = 152
      Width = 316
      Height = 24
      TabOrder = 4
      Text = 'Cel delegacji'
    end
    object EditDieta: TEdit
      Left = 440
      Top = 168
      Width = 121
      Height = 24
      TabOrder = 5
      Text = '0,00'
      OnExit = EditDietaExit
    end
    object Button1: TButton
      Left = 880
      Top = 72
      Width = 75
      Height = 25
      Caption = 'Nowa'
      TabOrder = 6
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 880
      Top = 136
      Width = 75
      Height = 25
      Caption = 'Edycja'
      TabOrder = 7
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 880
      Top = 168
      Width = 75
      Height = 25
      Caption = 'Zapisz'
      TabOrder = 8
      OnClick = Button3Click
    end
    object Button8: TButton
      Left = 880
      Top = 104
      Width = 75
      Height = 25
      Caption = 'Wczytaj'
      TabOrder = 9
      OnClick = Button8Click
    end
    object Edit1: TEdit
      Left = 88
      Top = 24
      Width = 121
      Height = 24
      TabOrder = 10
      Text = '0'
      OnExit = Edit1Exit
    end
    object Edit6: TEdit
      Left = 720
      Top = 40
      Width = 41
      Height = 24
      ReadOnly = True
      TabOrder = 11
      Text = '0'
    end
    object Edit9: TEdit
      Left = 800
      Top = 40
      Width = 41
      Height = 24
      ReadOnly = True
      TabOrder = 12
      Text = '0'
    end
    object Edit10: TEdit
      Left = 888
      Top = 40
      Width = 41
      Height = 24
      TabOrder = 13
      Text = '0'
    end
    object Edit11: TEdit
      Left = 784
      Top = 240
      Width = 177
      Height = 24
      Color = clYellow
      TabOrder = 14
      Text = '0'
    end
    object Button5: TButton
      Left = 664
      Top = 144
      Width = 121
      Height = 49
      Caption = 'Oblicz'
      TabOrder = 15
      OnClick = Button5Click
    end
    object Uwagi: TEdit
      Left = 72
      Top = 208
      Width = 321
      Height = 24
      TabOrder = 16
      Text = 'Uwagi'
    end
    object Button10: TButton
      Left = 880
      Top = 200
      Width = 75
      Height = 25
      Caption = 'Odrzuc'
      TabOrder = 17
      OnClick = Button10Click
    end
  end
  object gbDelegowany: TGroupBox
    Left = 0
    Top = 281
    Width = 433
    Height = 176
    Align = alCustom
    Caption = 'Delegowany'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    OnExit = gbDelegowanyExit
    object Label9: TLabel
      Left = 24
      Top = 96
      Width = 25
      Height = 16
      Caption = 'Imi'#281
    end
    object Label10: TLabel
      Left = 200
      Top = 96
      Width = 58
      Height = 16
      Caption = 'Nazwisko'
    end
    object Label11: TLabel
      Left = 32
      Top = 40
      Width = 13
      Height = 16
      Caption = 'ID'
    end
    object Edit3: TEdit
      Left = 56
      Top = 96
      Width = 121
      Height = 24
      TabOrder = 0
      Text = 'Imie'
    end
    object Edit4: TEdit
      Left = 264
      Top = 96
      Width = 121
      Height = 24
      TabOrder = 1
      Text = 'Nazwisko'
    end
    object Edit5: TEdit
      Left = 56
      Top = 40
      Width = 121
      Height = 24
      TabOrder = 2
      Text = '0'
      OnExit = Edit5Exit
    end
  end
  object gbTransport: TGroupBox
    Left = 440
    Top = 400
    Width = 569
    Height = 289
    Align = alCustom
    Caption = 'Transport'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    OnExit = gbTransportExit
    object Label26: TLabel
      Left = 357
      Top = 240
      Width = 64
      Height = 16
      Caption = 'Nale'#380'no'#347#263
    end
    object Label27: TLabel
      Left = 544
      Top = 240
      Width = 9
      Height = 16
      Caption = 'z'#322
    end
    object Edit8: TEdit
      Left = 432
      Top = 232
      Width = 105
      Height = 24
      Color = 7338586
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object sgTransport: TStringGrid
      Left = 16
      Top = 32
      Width = 321
      Height = 241
      ColCount = 3
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
      TabOrder = 1
      OnClick = sgTransportClick
      OnExit = sgTransportExit
      ColWidths = (
        41
        165
        81)
    end
    object Button4: TButton
      Left = 392
      Top = 32
      Width = 75
      Height = 25
      Caption = 'Dodaj'
      TabOrder = 2
      OnClick = Button4Click
    end
    object ComboBox1: TComboBox
      Left = 360
      Top = 144
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 3
      Visible = False
      OnChange = ComboBox1Change
      Items.Strings = (
        'Samoch'#243'd'
        'Autobus'
        'Poci'#261'g I klasa'
        'Poci'#261'g II klasa'
        'Samolot I klasa'
        'Samolot II klasa'
        'Statek')
    end
    object Button9: TButton
      Left = 392
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Usu'#324
      TabOrder = 4
      OnClick = Button9Click
    end
  end
  object gbNocleg: TGroupBox
    Left = 440
    Top = 281
    Width = 569
    Height = 112
    Align = alCustom
    Caption = 'Nocleg'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    OnExit = gbNoclegExit
    object Label17: TLabel
      Left = 120
      Top = 32
      Width = 106
      Height = 16
      Caption = 'Wysokosc faktury'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 544
      Top = 72
      Width = 9
      Height = 16
      Caption = 'z'#322
    end
    object Label23: TLabel
      Left = 360
      Top = 72
      Width = 64
      Height = 16
      Caption = 'Nale'#380'no'#347#263
    end
    object Label22: TLabel
      Left = 360
      Top = 32
      Width = 9
      Height = 16
      Caption = 'z'#322
    end
    object rbFaktura: TRadioButton
      Left = 24
      Top = 32
      Width = 81
      Height = 17
      Caption = 'Faktura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object rbRyczalt: TRadioButton
      Left = 24
      Top = 56
      Width = 73
      Height = 17
      Caption = 'Rycza'#322't'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object EdNocFaktura: TEdit
      Left = 232
      Top = 24
      Width = 121
      Height = 24
      TabOrder = 2
      Text = '0'
      OnExit = EdNocFakturaExit
    end
    object edNoclegRes: TEdit
      Left = 432
      Top = 64
      Width = 105
      Height = 24
      BiDiMode = bdLeftToRight
      Color = 7338586
      ParentBiDiMode = False
      ReadOnly = True
      TabOrder = 3
      Text = '0'
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 456
    Width = 433
    Height = 233
    Align = alCustom
    BiDiMode = bdRightToLeft
    Caption = 'Wy'#380'ywienie'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBiDiMode = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    OnExit = GroupBox1Exit
    object Label24: TLabel
      Left = 184
      Top = 192
      Width = 64
      Height = 16
      Caption = 'Nale'#380'no'#347#263
    end
    object Label25: TLabel
      Left = 384
      Top = 192
      Width = 9
      Height = 16
      Caption = 'z'#322
    end
    object rbWyzPosilki: TRadioButton
      Left = 24
      Top = 24
      Width = 257
      Height = 17
      Caption = 'Zapewnione ca'#322'odniowe wy'#380'ywienie'
      TabOrder = 0
    end
    object Edit7: TEdit
      Left = 256
      Top = 184
      Width = 121
      Height = 24
      Color = 7338586
      ReadOnly = True
      TabOrder = 1
      Text = '0'
    end
    object rbWyzWlasne: TRadioButton
      Left = 24
      Top = 48
      Width = 145
      Height = 17
      Caption = 'Wy'#380'ywienie w'#322'asne'
      TabOrder = 2
    end
  end
end

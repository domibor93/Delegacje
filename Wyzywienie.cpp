//---------------------------------------------------------------------------


#pragma hdrstop

#include "Wyzywienie.h"

//---------------------------------------------------------------------------
void Wyzywienie::LiczWyz(char tryb, int Dieta, int CzasDelGodz, int CzasDelDni)
{
	WyzTryb=tryb;
	switch(tryb)
	{
		case 'c':
				WyzSum=0;
				break;
		case 'w':
				if(CzasDelGodz>=8 && !CzasDelDni) // zgodnie z ustawa
				{
					if(CzasDelGodz>12)
						WyzSum=Dieta;
					else
						WyzSum=0.5*Dieta;
				}
				else if (!CzasDelDni)
					WyzSum=0;
				else
				{
					if (CzasDelGodz>8)      //zgodnie z ustawa
						WyzSum=(CzasDelDni+1)*Dieta;
					else
						WyzSum=(CzasDelDni+0.5)*Dieta;
				}		
				
				break;
		default:
				WyzTryb='c';
				WyzSum=0;
				break;
	}
	
}
//---------------------------------------------------------------------------
void Wyzywienie::SetWyz(TRadioButton *zapew, TRadioButton *wlasne, TEdit *Nalez, int dieta, int CzasDelGodz, int CzasDelDni)
{

	if(zapew->Checked)
		WyzTryb='c';
	else
		WyzTryb='w';
        LiczWyz(WyzTryb, dieta, CzasDelGodz, CzasDelDni);
}
//---------------------------------------------------------------------------

double Wyzywienie::GetWyzSum()
{
		return WyzSum;
}
//---------------------------------------------------------------------------
char Wyzywienie::GetWyzTryb()
{
		return WyzTryb;
}
//---------------------------------------------------------------------------

void Wyzywienie::PokazWyz(TRadioButton *zapew,TRadioButton *wlasne, TEdit *Nalez)
{
	Nalez->Text=FloatToStr(WyzSum);
	switch (WyzTryb)
	{
		case 'w':
				wlasne->Checked = true;
				zapew->Checked = false;
				break;
		case 'c':
				wlasne->Checked = false;
				zapew->Checked = true;
				break;
		default:
				wlasne->Checked = false;
				zapew->Checked = false;
	}
	
	
}
//---------------------------------------------------------------------------
#pragma package(smart_init)





//---------------------------------------------------------------------------

#ifndef DelegacjaH
#define DelegacjaH
#include <vcl.h>
#include <vector>
#include "Nocleg.h"
#include "Wyzywienie.h"
#include "Transport.h"
#include "Osoba.h"
#include "Unit1.h"

using namespace std;
//---------------------------------------------------------------------------
class Delegacja: public Nocleg, public Wyzywienie, public Transport
{
        private:
              int Nr;
              int NrDelegowanego; // to moze przyjaźnią ??
              double Dieta;
              double SumaSum;
              TDateTime DataOd;
              TDateTime DataDo;
              TDateTime GodzOd;
              TDateTime GodzDo;
              AnsiString Cel;
              AnsiString Opis;
              int Dni;
              int Godzin;
              int Minut;
              void LiczCzas();
              void SetOsobaID(Osoba o);
        public:
              void SetDel(
                          Osoba o,
                          TEdit *nrDel, TDateTimePicker *odData, TDateTimePicker *doData,
                          TDateTimePicker *odGodzina, TDateTimePicker *doGodzina,
                          TEdit *edCel, TEdit* edUwagi, TEdit *edDieta,
                          TStringGrid *grid,
                          TRadioButton *rbfak, TEdit *NocWart,
                          TRadioButton *rbWyzZap,TRadioButton *rbWyzWlas, TEdit *EdWyzNalez,
                          TEdit *osID, TEdit *osImie, TEdit *osNazwisko
                          );
              void PokazDel(
                            Osoba o,
                        TEdit *nrDel, TDateTimePicker *odData, TDateTimePicker *doData,
                        TDateTimePicker *odGodzina, TDateTimePicker *doGodzina,
                        TEdit *dn, TEdit *hrs, TEdit *mins,
                        TEdit *edCel, TEdit* edUwagi, TEdit *edDieta,
                        TStringGrid *grid,
                        TRadioButton *rbfak, TRadioButton *rbrycz, TEdit *NocWart, TEdit *nocSuma,
                        TRadioButton *rbWyzZap,TRadioButton *rbWyzWlas, TEdit *EdWyzNalez,
                        TEdit *osID, TEdit *osImie, TEdit *osNazwisko, TEdit *SumSum
                            );

};
//---------------------------------------------------------------------------
#endif

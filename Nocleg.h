//---------------------------------------------------------------------------

#ifndef NoclegH
#define NoclegH
//---------------------------------------------------------------------------
class Nocleg
{
        private:
                char NocTryb;
                double NoclegSum;
        public:
                void NoclegSet(TRadioButton *fak, double dieta ,double NocWart, int liczbaNocy); 
                void PokazNoc(TRadioButton *fak, TRadioButton *rycz, TEdit *faktura, TEdit *nocSuma);
                         //Wczytywanie na formatke

                double GetNocSum();      //wyciaganie naleznosci za nocleg z klasy
                char GetNocTryb();
};
//---------------------------------------------------------------------------
#endif

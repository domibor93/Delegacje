//---------------------------------------------------------------------------

#ifndef TransportH
#define TransportH
#include <vector.h>
#include <vcl.h>

//---------------------------------------------------------------------------

class Transport
{
        private:  
               vector < AnsiString > Srodki;   //wektor Srodkow Transportu
               vector < AnsiString > Koszty;
               double TransSum;
               void DodajEtap(AnsiString aSrodek, AnsiString aKoszt);
        public:
                void SetTrans(TStringGrid *tab);   //zapisz do obiektu to co jest w siatce
                void LoadTrans(TStringGrid *tab);  //wczytaj z obiektu do siatki
                double GetTransSum();


};
//---------------------------------------------------------------------------

#endif

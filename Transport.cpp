//---------------------------------------------------------------------------


#pragma hdrstop
#include "Transport.h"
#include <vcl.h>

//---------------------------------------------------------------------------
void Transport::DodajEtap(AnsiString aSrodek, AnsiString aKoszt)
{
		Srodki.push_back(aSrodek);
		Koszty.push_back(aKoszt);
		TransSum+=(aKoszt=="") ? 0 : StrToFloat(aKoszt);
}
//---------------------------------------------------------------------------
void Transport::SetTrans(TStringGrid *tab)
{
		Srodki.clear();
		Koszty.clear();
		TransSum=0;
		for(int k=1; k < tab->RowCount; k++)
		{
			DodajEtap(tab->Cells[1][k] , tab->Cells[2][k]);			
		}	
}
//---------------------------------------------------------------------------
void Transport::LoadTrans(TStringGrid *tab)
{
		for(int i=tab->RowCount-1; i>0; i--)
		{
			for (int j=0; j<tab->ColCount; j++)
			{
				tab->Cells[j][i]="";	
				
			}		
			if(i>1)
				tab->RowCount--;
			
		}
                double c=Srodki.size();
		for(int k=0; k<=Srodki.size()-1;k++)
		{
				if(k<Srodki.size()-1)
				{
					tab->RowCount++;
				}
				tab->Cells[0][k+1]=IntToStr(k+1);
				tab->Cells[1][k+1]=Srodki[k];
				tab->Cells[2][k+1]=Koszty[k];
		}	
}
//---------------------------------------------------------------------------
double Transport::GetTransSum()
{
	return TransSum;
}
#pragma package(smart_init)

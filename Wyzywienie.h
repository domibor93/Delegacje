//---------------------------------------------------------------------------

#ifndef WyzywienieH
#define WyzywienieH

#include <vcl.h>
//---------------------------------------------------------------------------
class Wyzywienie
{
        private:
            char WyzTryb;      //c zapewnione, w - wlasne
            double WyzSum; //naleznosc za wyzywienie
         public:
            void LiczWyz(char tryb, int Dieta, int CzasDelGodz, int CzasDelDni);
            void SetWyz(TRadioButton *zapew, TRadioButton *wlasne, TEdit *Nalez, int dieta, int CzasDelGodz, int CzasDelDni); //wzapisywanie z formatki do obietku (przeladowana nazwa)

            double GetWyzSum();
            char GetWyzTryb();
            void PokazWyz(TRadioButton *zapew,TRadioButton *wlasne, TEdit *Nalez); //wczytywanie z obektu na formatke


};
//---------------------------------------------------------------------------
#endif

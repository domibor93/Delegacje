//---------------------------------------------------------------------------


#pragma hdrstop

#pragma link "CSPIN"
#include "Delegacja.h"

//---------------------------------------------------------------------------
void Delegacja::SetDel(
                        Osoba o, TEdit *nrDel, TDateTimePicker *odData, TDateTimePicker *doData,
                        TDateTimePicker *odGodzina, TDateTimePicker *doGodzina,
                        TEdit *edCel,TEdit* edUwagi, TEdit *edDieta,
                        TStringGrid *grid,TRadioButton *rbfak, TEdit *NocWart,
                        TRadioButton *rbWyzZap,TRadioButton *rbWyzWlas, TEdit *EdWyzNalez,
                        TEdit *osID, TEdit *osImie, TEdit *osNazwisko
                        )
{
        Dieta=StrToFloat(edDieta->Text);
        DataOd=odData->Date.Val;
	DataDo=doData->Date.Val;
        GodzOd=odGodzina->Time;
        GodzDo=doGodzina->Time;
        LiczCzas();

	NoclegSet(rbfak, Dieta ,StrToFloat(NocWart->Text), Dni);
	SetTrans(grid);
	SetOsobaID(o);
        Nr=StrToInt(nrDel->Text);



	Cel=edCel->Text;
	Opis=edUwagi->Text;
        SetWyz(rbWyzZap, rbWyzWlas, EdWyzNalez, Dieta, Godzin, Dni);
        o.SetOsoba(osID, osImie, osNazwisko);

	SumaSum=GetWyzSum() + GetNocSum() + GetTransSum();
}
//TCSpinEdit *nrDel, 
//---------------------------------------------------------------------------
void Delegacja::PokazDel(
                        Osoba o,
                        TEdit *nrDel, TDateTimePicker *odData, TDateTimePicker *doData,
                        TDateTimePicker *odGodzina, TDateTimePicker *doGodzina,
                        TEdit *dn, TEdit *hrs, TEdit *mins,
                        TEdit *edCel, TEdit* edUwagi, TEdit *edDieta,
                        TStringGrid *grid,
                        TRadioButton *rbfak, TRadioButton *rbrycz, TEdit *NocWart, TEdit *nocSuma,
                        TRadioButton *rbWyzZap,TRadioButton *rbWyzWlas, TEdit *EdWyzNalez,
                        TEdit *osID, TEdit *osImie, TEdit *osNazwisko, TEdit *SumSum
                        )
{
	nrDel->Text=IntToStr(Nr);
        //Daty i godziny zrobic


        dn->Text=Dni;
        hrs->Text=Godzin;
        mins->Text=Minut;
	edCel->Text=Cel;
	edUwagi->Text=Opis;
        edDieta->Text=FloatToStr(Dieta);
	LoadTrans(grid);
	PokazNoc(rbfak, rbrycz, NocWart, nocSuma);
        PokazWyz(rbWyzZap, rbWyzWlas, EdWyzNalez);
        o.ShowOsoba(osID, osImie, osNazwisko);
        SumSum->Text=SumaSum;
	
}
//---------------------------------------------------------------------------
void Delegacja::SetOsobaID(Osoba o)
{
        NrDelegowanego=o.ID;
}
//---------------------------------------------------------------------------
void Delegacja::LiczCzas()                    //wszystkie funckje oparte na dokumentacji ze strony http://www.yevol.com/bcb/Lesson33.htm
{
        unsigned short int h1,h2,m1,m2,s1,s2,mm1,mm2; //potrzebne do rozkodowania daty, s-y i mm-y nie sa uzywane
        Dni=DataDo-DataOd;
        GodzOd.DecodeTime(&h1,&m1,&s1,&mm1);
        GodzDo.DecodeTime(&h2,&m2,&s2,&mm2);

        Godzin=h2-h1;
        Minut = m2-m1;

        if (Minut < 0)
        {
           Godzin--;
           Minut=60+Minut;   //ta sama zasada co nizej, jest mniejsze od zera wiec dodaje nie odejmuje
        }
        if (Godzin < 0)
        {
           Dni--;
           Godzin=24+Godzin; //jezeli godzina przybycia jest wczesniejsza od wyjazdu to nei ma calego dnia
         }
         if(Dni<0)
             Application->MessageBox("Data przyjazdu jest wczesniejsza niz wyjazdu.","Niespójnosc danych", MB_OK | MB_ICONASTERISK);


}
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------


#pragma hdrstop

#include <vcl.h>
#include "Nocleg.h"

//---------------------------------------------------------------------------
void Nocleg::NoclegSet(TRadioButton *fak, double dieta ,double NocWart, int liczbaNocy)
{
		
		NocTryb=(fak->Checked) ? 'f' : 'r' ;
		switch (NocTryb)
		{
			case 'f':
					NoclegSum=NocWart;
					break;
			case 'r':
					NoclegSum= liczbaNocy*dieta*1.5; //wg ustawy ryczalt za nocleg to 150% diety
					break;
			
		}

	 
 }
//---------------------------------------------------------------------------
void Nocleg::PokazNoc(TRadioButton *fak, TRadioButton *rycz, TEdit *faktura, TEdit *nocSuma)
{
		if(NocTryb=='f')
		{
				fak->Checked=true;
				rycz->Checked=false;
				faktura->Text=FloatToStr(NoclegSum);
                                nocSuma->Text=faktura->Text;         //jezeli faktura to suma jest rowna kwocie faktury
		}
	    else
		{
			   fak->Checked=false;
			   rycz->Checked=true;
			   faktura->Text=0;
                           nocSuma->Text=NoclegSum;
		}	
}
//---------------------------------------------------------------------------
double Nocleg::GetNocSum()
{
	return NoclegSum;
}
//---------------------------------------------------------------------------
char Nocleg::GetNocTryb()
{
	return NocTryb;
}
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "CGRID"
#pragma resource "*.dfm"
TForm1 *Form1;

const int Dieta = 30;         //ustawowa kwota diety
double sumaWyzywienie=0;
double sumaNoclegi=0;
double sumaTransport=0;
int nrTrans=1;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
        DateTime_SetFormat( dtpGodzinaOd->Handle, "HH:mm" ); // ustawianie formatu godziny na hh:mm, bo domyslnie sa jeszcze sekundy
        DateTime_SetFormat( dtpGodzinaDo->Handle, "HH:mm" );
        EditDieta->Text=Dieta;
        rbRyczalt->Checked=true; // domyslnie ustawiony ryczalt do rozliczenia noclegu
        rbWyzWlasne->Checked=true;
        sgTransport->Cells[0][0] = "Nr";
        sgTransport->Cells[1][0] = "Środek Transporu";
        sgTransport->Cells[2][0] = "Cena";
        sgTransport->Cells[0][1] = nrTrans;

        cbSniadanie  -> Enabled=false;
        cbObiad      -> Enabled=false;
        cbKolacja    -> Enabled=false;

}
//---------------------------------------------------------------------------



void __fastcall TForm1::Edit5Change(TObject *Sender)
{
        // jezeli id jest w bazie to wczytaj pozostale dane i zablokuj możliwosc edycji tych danych
        // jezeli nie to sprawdz najnizsze wolne id i zaproponuj je
}
//---------------------------------------------------------------------------






void __fastcall TForm1::EditDietaExit(TObject *Sender)
{
                double value;                                      //podpiac to do (zmienic na) zmiennej globalnej
    if( !TryStrToFloat(EditDieta->Text, value) )                      //konwersja string na double i zabezpieczenie przed zlymi danymi
    {                                                             //Diete należy wpisac z przecinkiem, NIE kropką
        Application->MessageBox("Wprowadz liczbę typu double.\nUżyj przecinka jako separatora.","Nieprawidłowa wartość", MB_OK | MB_ICONASTERISK);
        EditDieta->SetFocus();
        return;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{

       if (CheckBox1->Checked==true)
            EditDieta->Enabled = true;
       else
       {
            EditDieta->Enabled = false;
            EditDieta->Text=Dieta;
       }

}
//---------------------------------------------------------------------------






void __fastcall TForm1::rbFakturaClick(TObject *Sender)
{
        EdNocFaktura->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::rbRyczaltClick(TObject *Sender)
{
        EdNocFaktura->Enabled = false;

        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EdNocFakturaExit(TObject *Sender)
{
            double value;                                      //podpiac to do (zmienic na) zmiennej globalnej
    if( !TryStrToFloat(EdNocFaktura->Text, value) )                      //konwersja string na double i zabezpieczenie przed zlymi danymi
    {                                                             //Diete należy wpisac z przecinkiem, NIE kropką
        Application->MessageBox("Wprowadz liczbę typu double.\nUżyj przecinka jako separatora.","Nieprawidłowa wartość", MB_OK | MB_ICONASTERISK);
        EdNocFaktura->SetFocus();
        return;
    }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button4Click(TObject *Sender)                      //Dodawanie Nowego wiersza do string grid'a
{
               
              sgTransport->RowCount++;
              for(int i = sgTransport->RowCount - 1; i >= sgTransport->Row + 1; i--)
              {
                 for(int j = 0; j < sgTransport->ColCount; j++)
                 {
                   if(i == sgTransport->Row + 1)
                         sgTransport->Cells[j][sgTransport->Row + 1] = "";
                     else
                         sgTransport->Cells[j][i] = sgTransport->Cells[j][i - 1];
                  }

              }
              sgTransport->Cells[0][sgTransport->RowCount-1] = sgTransport->RowCount-1;


}
//---------------------------------------------------------------------------

void __fastcall TForm1::sgTransportClick(TObject *Sender)
{
        if( sgTransport->Col == 1 )                             //ustawianie comboboxa do kolumny Srodek transportu
         {
             TRect Recto     = sgTransport->CellRect(sgTransport->Col, sgTransport->Row);
                ComboBox1->Top  = sgTransport->Top;
                ComboBox1->Left = sgTransport->Left;
                ComboBox1->Top  = ComboBox1->Top + Recto.Top + sgTransport->GridLineWidth;
                ComboBox1->Left = ComboBox1->Left + Recto.Left + sgTransport->GridLineWidth + 1;
                ComboBox1->Height = (Recto.Bottom - Recto.Top) + 1;
                ComboBox1->Width  = Recto.Right - Recto.Left;
                ComboBox1->Visible = True;
         }
         else
                  ComboBox1->Visible = False;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{
        sgTransport->Cells[sgTransport->Col][sgTransport->Row] = ComboBox1->Items->Strings[ComboBox1->ItemIndex];
}
//---------------------------------------------------------------------------

void __fastcall TForm1::sgTransportExit(TObject *Sender)
{
     sumaTransport=0;
                for(int k=1; k < sgTransport->RowCount; k++)
                   {
                           sumaTransport+=(sgTransport->Cells[2][k]=="")? 0:StrToFloat(sgTransport->Cells[2][k]); // sprawdzanie czy pusta
                           Edit8->Text=sumaTransport;                                                            //komorka, jezeli tak to =0
                  }
}
//---------------------------------------------------------------------------



void __fastcall TForm1::rbWyzPosilkiClick(TObject *Sender)
{
        cbSniadanie  -> Enabled=true;
        cbObiad      -> Enabled=true;
        cbKolacja    -> Enabled=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::rbWyzWlasneClick(TObject *Sender)
{
        cbSniadanie  -> Enabled=false;
        cbObiad      -> Enabled=false;
        cbKolacja    -> Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox2Click(TObject *Sender)
{
         if(CheckBox2->Checked==true)
         {
                rbWyzWlasne->Enabled=false;
                rbWyzPosilki->Enabled=false;
                 cbSniadanie  -> Enabled=false;
                 cbObiad      -> Enabled=false;
                 cbKolacja    -> Enabled=false;
         }
         else
         {
                rbWyzWlasne->Enabled=true;
                rbWyzPosilki->Enabled=true;
                if(rbWyzPosilki->Checked==true)
                {
                        cbSniadanie  -> Enabled=true;
                        cbObiad      -> Enabled=true;
                        cbKolacja    -> Enabled=true;
                }
         }
}
//---------------------------------------------------------------------------








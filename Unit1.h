//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <FileCtrl.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include <Buttons.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include "CSPIN.h"
#include <ActnList.hpp>
#include "CGRID.h"
#include <Dialogs.hpp>
#include <iostream>

using namespace std;
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbDelegacja;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TDateTimePicker *DateTimePicker1;
        TDateTimePicker *DateTimePicker2;
        TDateTimePicker *dtpGodzinaOd;
        TDateTimePicker *dtpGodzinaDo;
        TEdit *Edit2;
        TLabel *Label7;
        TLabel *Label8;
        TGroupBox *gbDelegowany;
        TEdit *Edit3;
        TEdit *Edit4;
        TEdit *Edit5;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TGroupBox *gbTransport;
        TLabel *Label12;
        TEdit *EditDieta;
        TGroupBox *gbNocleg;
        TGroupBox *GroupBox1;
        TLabel *Label13;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TRadioButton *rbFaktura;
        TRadioButton *rbRyczalt;
        TEdit *EdNocFaktura;
        TLabel *Label17;
        TLabel *Label21;
        TLabel *Label23;
        TEdit *edNoclegRes;
        TLabel *Label22;
        TRadioButton *rbWyzPosilki;
        TEdit *Edit7;
        TLabel *Label24;
        TLabel *Label25;
        TLabel *Label26;
        TEdit *Edit8;
        TLabel *Label27;
        TStringGrid *sgTransport;
        TButton *Button4;
        TComboBox *ComboBox1;
        TRadioButton *rbWyzWlasne;
        TButton *Button8;
        TButton *Button9;
        TEdit *Edit1;
        TEdit *Edit6;
        TEdit *Edit9;
        TEdit *Edit10;
        TLabel *Label6;
        TLabel *Label14;
        TLabel *Label15;
        TEdit *Edit11;
        TLabel *Label16;
        TButton *Button5;
        TEdit *Uwagi;
        TButton *Button10;
        void __fastcall ComboBox1Change(TObject *Sender);
        void __fastcall sgTransportClick(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall EditDietaExit(TObject *Sender);
        void __fastcall Edit5Exit(TObject *Sender);
        void __fastcall EdNocFakturaExit(TObject *Sender);
        void __fastcall sgTransportExit(TObject *Sender);
        void __fastcall Button8Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button9Click(TObject *Sender);
        void __fastcall Edit1Exit(TObject *Sender);
        void __fastcall Init();            //<-te funkcje zostaly napisane, reszta generowana automatycznie
        void __fastcall Zapisz ();          //
        void __fastcall Wczytaj();         //
        void __fastcall WylEdycje();         //
        void __fastcall WlEdycje();       //
        void __fastcall ZapiszDoPliku ();   //
        void __fastcall WczytajZpliku (); //
        string __fastcall UsunSpacje(string str);   //
        string __fastcall PrzywrocSpacje(string str);    //
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall gbDelegacjaExit(TObject *Sender);
        void __fastcall gbDelegowanyExit(TObject *Sender);
        void __fastcall GroupBox1Exit(TObject *Sender);
        void __fastcall gbTransportExit(TObject *Sender);
        void __fastcall gbNoclegExit(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button10Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

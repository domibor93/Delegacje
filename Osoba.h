//---------------------------------------------------------------------------

#ifndef OsobaH
#define OsobaH
#include <vcl.h>
#include "Unit1.h"
//---------------------------------------------------------------------------
class Osoba
{
	private:
		int ID;
		AnsiString Imie;
                AnsiString Nazwisko;  //borland zamiast Stringow uzywa AnsiStringow
		                        //latwo mozna pododawac nowe pola typu Adress itp
	public:
                Osoba (int aID, AnsiString aImie, AnsiString aNazwisko); //<- konstruktor
	        void SetOsoba(TEdit *eID, TEdit *eImie, TEdit *eNazwisko); //zapis danych do obiektu z formatki
																							     //konstruktorem mozna zapisac bezposrednio
                void ShowOsoba(TEdit *eID, TEdit *eImie, TEdit *eNazwisko);     //wyswietla parametry obiektu w Edit boxach podanych jako parametry funkcji

                friend class Delegacja;

};
//---------------------------------------------------------------------------
#endif
